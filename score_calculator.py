import os
import db
import spacy
import pandas as pd
# compares solution and answer based on the developped language model
# param: solution (string), answer (string)
# return: score (real number between 0 and 1)
import en_core_web_lg


def calculate_score(solution, answer, text2KG_object, kg2score_object):
    # get KG from text
    solutionKG, answerKG = text2KG_object.get_solution_answer(solution, answer)

    # get scroe from kgs
    score = kg2score_object.get_score_for_solution_answer(solutionKG, answerKG)

    return score

# calculates grade for a given answer to a question
# param: questionID (number), answer (string)
# return: grade (real number between 1 and 6)


def grade_answer(question, answer, text2KG_object, kg2score_object):
    solution = db.get_solution(question)["data"]

    print("Question Text", question)
    print("Answer Text", answer)
    print("Solution Text", solution)

    if answer[-1] == ".":
        answer = answer[0:len(answer)-2]

    # assuming score as real number between 0 (worst answer) and 1 (perfect answer)
    question_score = calculate_score(
        solution, answer, text2KG_object, kg2score_object)

    # german highschool grading: best grade 1 worst grade 6
    grade = 1.0 + ((1.0 - question_score) * 5.0)
    return grade


class KG2Score:

    def __init__(self):
        # todo move all spacy to text2kg
        self.nlp = en_core_web_lg.load()

    def triples_to_embedded_triple(self, triple_pandas_df):
        return triple_pandas_df.applymap(lambda x: self.nlp(x))

    def compare_graphs(self, embedded_solution, embedded_answer):
        # find most similar triple in answer for each triple
        return embedded_solution.apply(lambda row: self.find_best_answer(row, embedded_answer), axis=1)

    def find_best_answer(self, solution_triple, embedded_answer):
        # calculate array of similarity for all answer triples to current solution triple
        print("\tSimilarity:")
        distances = embedded_answer.apply(
            lambda row: self.compare_triple_language_distance(solution_triple, row), axis=1)
        highest_relevance = max(distances["similarity"])
        most_relevant_index = distances["similarity"].argmax()
        return pd.Series([most_relevant_index, highest_relevance], index=['most_relevant_answer_triple_index', 'similarity_language_model'])

    def compare_triple_language_distance(self, solution_triple, answer_triple):
        sub_sim = solution_triple["subject"].similarity(
            answer_triple["subject"])
        rel_sim = solution_triple["relation"].similarity(
            answer_triple["relation"])
        obj_sim = solution_triple["object"].similarity(answer_triple["object"])
        print("\t\tSubject: {}\n\t\tRelation: {}\n\t\tObject:".format(
            sub_sim, rel_sim, obj_sim))
        # TODO optimise
        return pd.Series([(sub_sim + rel_sim + obj_sim) / 3], index=['similarity'])

    def calculate_final_score(self, similar_answers_for_solutions_kg_df):
        # calculate the final score
        similarity_vector = similar_answers_for_solutions_kg_df['similarity_language_model'].to_numpy(
        )
        length_solutions = len(similarity_vector)
        length_right_answers = len(similarity_vector[similarity_vector > 0.67])

        final_score = length_right_answers / length_solutions
        print("\tFinal similarity score:", final_score, "<---------")
        return final_score

    def get_score_for_solution_answer(self, solution_kg, answer_kg):
        print("Solution KG:", solution_kg)
        print("Answer KG:", answer_kg)
        # Embed the data frames to vector space
        solution_df_embedded = self.triples_to_embedded_triple(solution_kg)
        answer_df_embedded = self.triples_to_embedded_triple(answer_kg)

        # compare data
        similar_answers_for_solutions_kg_df = self.compare_graphs(
            solution_df_embedded, answer_df_embedded)

        # calculate final score
        q_score = self.calculate_final_score(
            similar_answers_for_solutions_kg_df)

        return q_score
