# NLP Seminar Project

# Development
In order to run the code locally first install dependencies with:

`pip install -r requirements.txt`

Then run the app with:

`python app.py`

## Branching
- master is the deployed production branch. Only merge into master if code is tested and production ready.
- develop is the development branch and is not deployed. Use this branch and feature branches to develop.

# Deployment
1. For deployment choose a VM provider of your choice and get a VM up and running.
2. You need to install Docker, either you are clever and spin up a VM with Docker already in the machine image or install it following the instructions on the official website.
2. On your VM, clone the repo using `git clone https://gitlab.com/Tdsone/final_nlp_main.git`
4. `cd` into the project folder and build the image: `docker build -t nlp-project .`
5. Spin up a container based on that image: `docker run -d -p 5000:5000 nlp-project`
6. Check that the container is running: `docker ps`

# User Interaction
### Admin Site / Teacher
The python backend serves a website on the path '/', which can be used to add questions to the database. Each question consists of Topic, Question and Answer.<br />
(This website is written in VueJs)
### User / Student
The user invokes the skill (on Alexa) and has the can start an exam by telling Alexa the topic of the exam. He is asked the exam questions one by one and can answer.
After all questions are answered, he will get a grade by Alexa for every single question as well as an average score.

# Architecture
![Architecture](https://pitch-assets-ccb95893-de3f-4266-973c-20049231b248.s3.eu-west-1.amazonaws.com/9a053299-010b-4196-b6a3-5399b24b83bd?pitch-bytes=118204&pitch-content-type=image/png)
The application consists of different parts: 
- a Lambda Function to handle user intents made with Alexa
- a Flask app that lives inside a Docker Container that is deployed on a AWS EC2 instance
- a reverse proxy (NGINX) that routes requests from pport 80 to 5000 (flask app)
- an in-memory "database"

## Flask API (Docker Container on EC2)

The Docker container is build on a python image and makes the Flask REST API available via port 5000. There are different routes that the API provides the Alexa application with: 

 - `GET /` sends back a simple HTML file where new exam questions can be posted
 - `GET /start-exam/<topic>` sends back all questions that are stored for a certain topic
 - `POST /grade-exam` requires a JSON body of the format `{"questions": ["question1", ...], "answers": ["answer1", ...]}` and sends back an overall grade and a specific grade for each question in the following format: `{"data": {
 "final_grade": final_grade,
 "grades": grades
 },
 "has_errored": False
 }`
 - `POST /questions`: to create a new question

 

# The NLP Stuff
The core NLP challenge of this project is to compare to answers to the same question with each other in a meaningful way.
We do this by extracting a knowledge graph out of the students answer and the teachers solution. 
Then the two knowledge graphs are compared. 

## How we generate a knowledge graph from text 
### Input
The input is either a text from the solution database or the users answer, as recorded by Amazon Alexa. Both times, it is an english text.
### Process
The text is processed in three steps:
1) Clean-up: Removing punctuation and new-lines (and html code, for web-scraping solution texts from e.g. Wikipedia), filling an array with the single sentences.
2) Subject and Object and Verb (plus preposition) are extracted using spaCy
3) Additional improvements: 'It' (subject) is replaced by the subject of the previous sentence. Sentences without recognized subject or object are removed.
4) (Optional) Each Subject is connected as object with a given subject (e.g. the title) and a given verb (e.g. 'part of') This can be decided by the developer and might improve the comparability between knowledge graphs.
### Output
A knowledge graph in the form of a DataFrame (Columns: Subject, Object, Relation)


## How we compare two knowledge graphs
The knowledge graph comparison takes two graphs in the form of dataframes with the triples as rows as input. One of the graphs is the solution and the other the answer of the person taking the quiz. We then look if there exist a similar triple, for each triple of the solution, in the answer graph. This is done by embedding the triples with a language model into vector space and then calculating a distance between the triples. The distance/ similarity is then used in order to calculate the final score.

# Examples
## Knowledge Graph from Text
For this example, we crested two Knowledge Graphs on base of Wikipedia articles (just using the first 100 sentences). Since a DataFrame is not very appealing to the eye, we created graphs.
### Typescript
![TypeScript](https://gitlab.com/Tdsone/final-nlp-main/-/raw/master/examples/typescript.png)
### Siegmund Freud
![Siegmund Freud](https://gitlab.com/Tdsone/final-nlp-main/-/raw/master/examples/freud.png)

## Generating Score by comparing two Knowledge Graphs


# Other interesting learnings
- AWS Lambda has a max. size of 50MB of code, which makes it impractical for machine learning deployment. It's best to know this before you build a CI/CD pipline :).
